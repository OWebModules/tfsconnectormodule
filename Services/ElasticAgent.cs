﻿using Microsoft.TeamFoundation.WorkItemTracking.WebApi.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Converters;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TFSConnectorModule.Models;

namespace TFSConnectorModule.Services
{
    public class ElasticAgent : ElasticBaseAgent
    {

        public async Task<ResultItem<CreateChangeSetsResult>> CreateChangeSets(CreateChangeSetsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<CreateChangeSetsResult>>(() =>
            {
                var relationConfig = new clsRelationConfig(globals);
                var result = new ResultItem<CreateChangeSetsResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new CreateChangeSetsResult()

                };

                result.Result.ChangeSets = request.ChangeSets.Select(req =>
                {
                    var changeSet = new clsOntologyItem
                    {
                        GUID = globals.NewGUID,
                        Name = req.ChangesetId.ToString(),
                        GUID_Parent = Config.LocalData.Class_Changeset.GUID,
                        Type = globals.Type_Object
                    };

                    var url = new clsOntologyItem
                    {
                        GUID = globals.NewGUID,
                        Name = req.Url,
                        GUID_Parent = Config.LocalData.Class_Url.GUID,
                        Type = globals.Type_Object
                    };

                    var description = new clsObjectAtt
                    {
                        ID_Attribute = globals.NewGUID,
                        ID_AttributeType = Config.LocalData.AttributeType_Description.GUID,
                        ID_Object = changeSet.GUID,
                        ID_Class = changeSet.GUID_Parent,
                        ID_DataType = Config.LocalData.AttributeType_Description.GUID_Parent,
                        OrderID = 1,
                        Val_String = req.Comment ?? "n.a.",
                        Val_Name = !string.IsNullOrEmpty(req.Comment) ? req.Comment.Length > 255 ? req.Comment.Substring(0, 254) : req.Comment : "n.a."
                    };

                    var createDate = new clsObjectAtt
                    {
                        ID_Attribute = globals.NewGUID,
                        ID_AttributeType = Config.LocalData.AttributeType_Datetimestamp__Create_.GUID,
                        ID_Object = changeSet.GUID,
                        ID_Class = changeSet.GUID_Parent,
                        ID_DataType = Config.LocalData.AttributeType_Datetimestamp__Create_.GUID_Parent,
                        OrderID = 1,
                        Val_Datetime = req.CreatedDate,
                        Val_Name = req.CreatedDate.ToString()
                    };



                    var changeSetDb = new ChangeSetDb
                    {
                        ChangeSet = changeSet,
                        Url = url,
                        Description = description,
                        CreateDate = createDate,
                        RelToProject = relationConfig.Rel_ObjectRelation(changeSet, request.Project, Config.LocalData.RelationType_belongs_to),
                        RelToUrl = relationConfig.Rel_ObjectRelation(changeSet, url, Config.LocalData.RelationType_belonging_Source)
                    };

                    return changeSetDb;
                }).ToList();

                var dbWriter = new OntologyModDBConnector(globals);


                var saveObjects = result.Result.ChangeSets.Select(change => change.ChangeSet).ToList();
                saveObjects.AddRange(result.Result.ChangeSets.Select(change => change.Url));

                if (saveObjects.Any())
                {
                    result.ResultState = dbWriter.SaveObjects(saveObjects);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving Objects";
                        return result;
                    }
                }


                var saveObjAtt = result.Result.ChangeSets.Select(change => change.CreateDate).ToList();
                saveObjAtt.AddRange(result.Result.ChangeSets.Select(change => change.Description));

                if (saveObjAtt.Any())
                {
                    result.ResultState = dbWriter.SaveObjAtt(saveObjAtt);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving Object-Attributes";
                        return result;
                    }
                }


                var saveRel = result.Result.ChangeSets.Select(change => change.RelToProject).ToList();
                saveRel.AddRange(result.Result.ChangeSets.Select(change => change.RelToUrl));

                if (saveRel.Any())
                {
                    result.ResultState = dbWriter.SaveObjRel(saveRel);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving Object-Relations";
                        return result;
                    }
                }


                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<ChangeSetDb>>> GetChangeSets(GetTFSCommentsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<ChangeSetDb>>>(() =>
            {
                var result = new ResultItem<List<ChangeSetDb>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<ChangeSetDb>()
                };

                var searchChangeSets = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = Config.LocalData.Class_Changeset.GUID
                    }
                };

                var dbReaderChangeSets = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderChangeSets.GetDataObjects(searchChangeSets);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the changesets!";
                    return result;
                }


                var searchAttributes = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Class = Config.LocalData.ClassAtt_Changeset_Datetimestamp__Create_.ID_Class,
                        ID_AttributeType = Config.LocalData.ClassAtt_Changeset_Datetimestamp__Create_.ID_AttributeType
                    }
                };

                var dbReaderCreate = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderCreate.GetDataObjectAtt(searchAttributes);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the create-Dates!";
                    return result;
                }

                var searchDescription = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Class = Config.LocalData.ClassAtt_Changeset_Description.ID_Class,
                        ID_AttributeType = Config.LocalData.ClassAtt_Changeset_Description.ID_AttributeType
                    }
                };

                var dbReaderDescription = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderDescription.GetDataObjectAtt(searchDescription);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the descriptions!";
                    return result;
                }

                var searchUrl = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = Config.LocalData.ClassRel_Changeset_belonging_Source_Url.ID_Class_Left,
                        ID_RelationType = Config.LocalData.ClassRel_Changeset_belonging_Source_Url.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Changeset_belonging_Source_Url.ID_Class_Right
                    }
                };

                var dbReaderUrl = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderUrl.GetDataObjectRel(searchUrl);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the urls!";
                    return result;
                }

                var descriptions = dbReaderDescription.ObjAtts;

                if (!string.IsNullOrEmpty(request.RegexComment))
                {
                    var regex = new Regex(request.RegexComment);
                    descriptions = descriptions.Where(desc => regex.Match(desc.Val_String).Success).ToList();
                }

                result.Result = (from changeSet in dbReaderChangeSets.Objects1
                                 join createDate in dbReaderCreate.ObjAtts on changeSet.GUID equals createDate.ID_Object
                                 join message in descriptions on changeSet.GUID equals message.ID_Object
                                 join url in dbReaderUrl.ObjectRels on changeSet.GUID equals url.ID_Object
                                 select new ChangeSetDb
                                 {
                                     ChangeSet = changeSet,
                                     CreateDate = createDate,
                                     Description = message,
                                     Url = new clsOntologyItem
                                     {
                                         GUID = url.ID_Other,
                                         Name = url.Name_Other,
                                         GUID_Parent = url.ID_Parent_Other,
                                         Type = url.Ontology
                                     }
                                 }).ToList();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<GetSyncModelResult>> GetSyncModel(IGetModelTFSConfig request)
        {
            var taskResult = await Task.Run<ResultItem<GetSyncModelResult>>(() =>
            {
                var result = new ResultItem<GetSyncModelResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetSyncModelResult()
                };

                if (string.IsNullOrEmpty(request.IdConfig) || !globals.is_GUID(request.IdConfig))
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "The provided Config-Id is not valid!";
                    return result;
                }

                var searchSubConfigs = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = request.IdConfig,
                        ID_RelationType = Config.LocalData.ClassRel_TFSConnectorModule_contains_TFSConnectorModule.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_TFSConnectorModule_contains_TFSConnectorModule.ID_Class_Right
                    }
                };


                var dbReaderConfigs = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigs.GetDataObjectRel(searchSubConfigs);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading Sub-configs";
                    return result;
                }

                result.Result.SubConfigList = dbReaderConfigs.ObjectRels;
                result.Result.ConfigList = result.Result.SubConfigList.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).ToList();

                if (!result.Result.ConfigList.Any())
                {
                    var searchConfig = new List<clsOntologyItem>
                    {
                        new clsOntologyItem
                        {
                            GUID = request.IdConfig
                        }
                    };

                    result.ResultState = dbReaderConfigs.GetDataObjects(searchConfig);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while Config";
                        return result;
                    }

                    result.Result.ConfigList.Add(dbReaderConfigs.Objects1.FirstOrDefault());
                }

                if (!result.Result.ConfigList.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Config found!";
                    return result;

                }

                var searchUrls = result.Result.ConfigList.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_TFSConnectorModule_belonging_Source_Url.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_TFSConnectorModule_belonging_Source_Url.ID_Class_Right
                }).ToList();

                var dbReaderConfigsToUrls = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigsToUrls.GetDataObjectRel(searchUrls);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading Urls";
                    return result;
                }

                result.Result.ConfigsToUrls = dbReaderConfigsToUrls.ObjectRels;

                if (!result.Result.ConfigsToUrls.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Url found!";
                    return result;
                }

                var searchPostFixUrls = result.Result.ConfigList.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_TFSConnectorModule_Issue_Postfix_Url.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_TFSConnectorModule_Issue_Postfix_Url.ID_Class_Right
                }).ToList();

                var dbReaderConfigsToPostFixUrls = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigsToPostFixUrls.GetDataObjectRel(searchPostFixUrls);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Postfix-Url!";
                    return result;
                }

                result.Result.ConfigsToPostFixUrls = dbReaderConfigsToPostFixUrls.ObjectRels;

                if (result.Result.ConfigsToPostFixUrls.Count != result.Result.ConfigList.Count)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"{result.Result.ConfigList.Count} Configs, but {result.Result.ConfigsToPostFixUrls.Count} Postfix-Urls!";
                    return result;
                }

                var searchProjects = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = Config.LocalData.Class_Project__TFS_Azure_DevOps_.GUID
                    }
                };

                var dbReaderProjects = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderProjects.GetDataObjects(searchProjects);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading Projects";
                    return result;
                }
                result.Result.Projects = dbReaderProjects.Objects1;

                var searchFilterProject = result.Result.ConfigList.Select(cfg => new clsObjectRel
                {
                    ID_Object = cfg.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_TFSConnectorModule_belonging_Project__TFS_Azure_DevOps_.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_TFSConnectorModule_belonging_Project__TFS_Azure_DevOps_.ID_Class_Right
                }).ToList();

                var dbReaderFilterProject = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderFilterProject.GetDataObjectRel(searchFilterProject);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading Filter-Project";
                    return result;
                }

                result.Result.ConfigToProject = dbReaderFilterProject.ObjectRels;

                List<clsObjectRel> searchReleases = new List<clsObjectRel>();

                if (result.Result.ConfigToProject.Any())
                {
                    searchReleases = result.Result.ConfigToProject.Select(rel => new clsObjectRel
                    {
                        ID_Object = rel.ID_Other,
                        ID_RelationType = Config.LocalData.ClassRel_Project__TFS_Azure_DevOps__contains_Release.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Project__TFS_Azure_DevOps__contains_Release.ID_Class_Right,
                    }).ToList();
                }
                else
                {
                    searchReleases = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Parent_Object = Config.LocalData.ClassRel_Project__TFS_Azure_DevOps__contains_Release.ID_Class_Left,
                            ID_RelationType = Config.LocalData.ClassRel_Project__TFS_Azure_DevOps__contains_Release.ID_RelationType,
                            ID_Parent_Other = Config.LocalData.ClassRel_Project__TFS_Azure_DevOps__contains_Release.ID_Class_Right,
                        }
                    };
                }


                if (searchReleases.Any())
                {
                    var dbReaderReleases = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderReleases.GetDataObjectRel(searchReleases);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading Releases";
                        return result;
                    }

                    result.Result.ProjectsToReleases = dbReaderReleases.ObjectRels;
                }

                var searchChangeSetsToProjects = new List<clsObjectRel>();

                if (result.Result.ConfigToProject.Any())
                {
                    searchChangeSetsToProjects = result.Result.ConfigToProject.Select(rel => new clsObjectRel
                    {
                        ID_Other = rel.ID_Other,
                        ID_RelationType = Config.LocalData.ClassRel_Changeset_belongs_to_Project__TFS_Azure_DevOps_.ID_RelationType,
                        ID_Parent_Object = Config.LocalData.ClassRel_Changeset_belongs_to_Project__TFS_Azure_DevOps_.ID_Class_Left
                    }).ToList();
                }
                else
                {
                    searchChangeSetsToProjects = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Parent_Object = Config.LocalData.ClassRel_Changeset_belongs_to_Project__TFS_Azure_DevOps_.ID_Class_Left,
                            ID_RelationType = Config.LocalData.ClassRel_Changeset_belongs_to_Project__TFS_Azure_DevOps_.ID_RelationType,
                            ID_Parent_Other = Config.LocalData.ClassRel_Changeset_belongs_to_Project__TFS_Azure_DevOps_.ID_Class_Right
                        }
                    };
                }

                if (searchChangeSetsToProjects.Any())
                {
                    var dbReaderChangeSetsToProjects = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderChangeSetsToProjects.GetDataObjectRel(searchChangeSetsToProjects);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading Changesets";
                        return result;
                    }

                    result.Result.ChangeSetsToProjects = dbReaderChangeSetsToProjects.ObjectRels;
                }

                var searchAccessTokens = result.Result.ConfigList.Select(cnf => new clsObjectRel
                {
                    ID_Object = cnf.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_TFSConnectorModule_secured_by_Accesstoken.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_TFSConnectorModule_secured_by_Accesstoken.ID_Class_Right
                }).ToList();

                var dbReaderAccessTokens = new OntologyModDBConnector(globals);

                if (searchAccessTokens.Any())
                {
                    result.ResultState = dbReaderAccessTokens.GetDataObjectRel(searchAccessTokens);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the AccessToken";
                        return result;
                    }

                    result.Result.ConfigToAccessTokens = dbReaderAccessTokens.ObjectRels;
                }

                var searchAttributes = result.Result.ConfigList.Select(cnf => new clsObjectAtt
                {
                    ID_Object = cnf.GUID,
                    ID_AttributeType = Config.LocalData.ClassAtt_TFSConnectorModule_Get_Changesets.ID_AttributeType
                }).ToList();

                searchAttributes.AddRange(result.Result.ConfigList.Select(cnf => new clsObjectAtt
                {
                    ID_Object = cnf.GUID,
                    ID_AttributeType = Config.LocalData.ClassAtt_TFSConnectorModule_Get_Workitems.ID_AttributeType
                }));


                var dbReaderGetAttributes = new OntologyModDBConnector(globals);

                if (searchAttributes.Any())
                {
                    result.ResultState = dbReaderGetAttributes.GetDataObjectAtt(searchAttributes);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Attributes of config!";
                        return result;
                    }

                    result.Result.ConfigToGetChangeSets = dbReaderGetAttributes.ObjAtts.Where(att => att.ID_AttributeType == Config.LocalData.ClassAtt_TFSConnectorModule_Get_Changesets.ID_AttributeType).ToList();
                    result.Result.ConfigToGetWorkItems = dbReaderGetAttributes.ObjAtts.Where(att => att.ID_AttributeType == Config.LocalData.ClassAtt_TFSConnectorModule_Get_Workitems.ID_AttributeType).ToList();
                }

                var searchAssignedTo = result.Result.ConfigList.Select(cfg => new clsObjectRel
                {
                    ID_Object = cfg.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_TFSConnectorModule_uses_AssignedTo__Azure_DevOps_.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_TFSConnectorModule_uses_AssignedTo__Azure_DevOps_.ID_Class_Right
                }).ToList();

                var dbReaderAssignedTo = new OntologyModDBConnector(globals);

                if (searchAssignedTo.Any())
                {
                    result.ResultState = dbReaderAssignedTo.GetDataObjectRel(searchAssignedTo);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the AssignedTos of configs!";
                        return result;
                    }

                }

                result.Result.ConfigsToAssignedTo = dbReaderAssignedTo.ObjectRels;

                if (result.Result.ConfigsToAssignedTo.Count != result.Result.ConfigList.Count)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "The number of assigned to is different to the number of configs!";
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<Models.Issue>>> CheckIssues(List<Models.Issue> issues, clsOntologyItem projectItem)
        {
            var taskResult = await Task.Run<ResultItem<List<Models.Issue>>>(() =>
           {
               var result = new ResultItem<List<Models.Issue>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = issues
               };

               var syncStamp = DateTime.Now;
               var syncSession = new clsOntologyItem
               {
                   GUID = globals.NewGUID,
                   Name = syncStamp.ToString("yyyy-MM-dd HH:mm:ss"),
                   GUID_Parent = Config.LocalData.Class_Sync_Session__TFS_Azure_.GUID,
                   Type = globals.Type_Object
               };

               var searchUrl = issues.Select(issue => new clsOntologyItem
               {
                   Name = issue.Url,
                   GUID_Parent = Config.LocalData.Class_Url.GUID
               }).ToList();

               var dbConnector = new OntologyModDBConnector(globals);

               if (searchUrl.Any())
               {
                   result.ResultState = dbConnector.GetDataObjects(searchUrl);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Urls!";
                       return result;
                   }
               }

               (from urlToDo in searchUrl
                join urlFound in dbConnector.Objects1 on urlToDo.Name equals urlFound.Name into urlsFound
                from urlFound in urlsFound.DefaultIfEmpty()
                select new { urlToDo, urlFound }).ToList().ForEach(url =>
                  {
                      url.urlToDo.GUID = url.urlFound != null ? url.urlFound.GUID : globals.NewGUID;
                      url.urlToDo.Mark = url.urlFound == null;
                      url.urlToDo.Type = globals.Type_Object;
                  });

               var urlsToSave = searchUrl.Where(url => url.Mark.Value).ToList();

               if (urlsToSave.Any())
               {
                   result.ResultState = dbConnector.SaveObjects(urlsToSave);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while saving new Urls!";
                       return result;
                   }
                   urlsToSave.ForEach(url => url.Mark = false);
               }

               (from urlDb in searchUrl
                join issue in issues on urlDb.Name equals issue.Url
                select new { urlDb, issue }).ToList().ForEach(url =>
                 {
                     url.issue.IdUrl = url.urlDb.GUID;
                 });

               var issuesToSave = issues.Select(issue => new clsOntologyItem
               {
                   GUID = issue.IdIssue,
                   Name = issue.NameIssue,
                   GUID_Parent = Config.LocalData.Class_Issue__Jira_.GUID,
                   Type = globals.Type_Object
               }).ToList();
               var objectsToSave = new List<clsOntologyItem>();
               objectsToSave.AddRange(issuesToSave);

               objectsToSave.AddRange(issues.Select(issue => new clsOntologyItem
               {
                   GUID = issue.IdTaskId,
                   Name = issue.NameTaskId,
                   GUID_Parent = Config.LocalData.Class_Task_Id.GUID,
                   Type = globals.Type_Object
               }));

               objectsToSave.Add(syncSession);

               if (objectsToSave.Any())
               {
                   result.ResultState = dbConnector.SaveObjects(objectsToSave);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while saving new Issues and TaskIds!";
                       return result;
                   }
               }

               var relationConfig = new clsRelationConfig(globals);

               var projects = issues.GroupBy(issue => new { issue.IdProject, issue.NameProject }).Select(prj => new clsOntologyItem
               {
                   GUID = prj.Key.IdProject,
                   Name = prj.Key.NameProject,
                   GUID_Parent = Config.LocalData.ClassRel_Project__TFS_Azure_DevOps__contains_Issue__Jira_.ID_Class_Left,
                   Type = globals.Type_Object
               }).ToList();
               if (!projects.Any(proj => proj.GUID == projectItem.GUID))
               {
                   projects.Add(projectItem);
               }

               var relationsToSave = (from project in projects
                                      join issue in issues on project.GUID equals issue.IdProject
                                      select new { project, issue }).Select(projIssue => relationConfig.Rel_ObjectRelation(projIssue.project, new clsOntologyItem
                                      {
                                          GUID = projIssue.issue.IdIssue,
                                          Name = projIssue.issue.NameIssue,
                                          GUID_Parent = Config.LocalData.ClassRel_Project__TFS_Azure_DevOps__contains_Issue__Jira_.ID_Class_Right,
                                          Type = globals.Type_Object
                                      }, Config.LocalData.RelationType_contains)).ToList();

               relationsToSave.AddRange(issues.Select(issue => relationConfig.Rel_ObjectRelation(new clsOntologyItem
               {
                   GUID = issue.IdIssue,
                   Name = issue.NameIssue,
                   GUID_Parent = Config.LocalData.ClassRel_Issue__Jira__is_defined_by_Task_Id.ID_Class_Left,
                   Type = globals.Type_Object
               }, new clsOntologyItem
               {
                   GUID = issue.IdTaskId,
                   Name = issue.NameTaskId,
                   GUID_Parent = Config.LocalData.ClassRel_Issue__Jira__is_defined_by_Task_Id.ID_Class_Right,
                   Type = globals.Type_Object
               },
               Config.LocalData.RelationType_is_defined_by)));

               relationsToSave.AddRange(issues.Select(issue => relationConfig.Rel_ObjectRelation(new clsOntologyItem
               {
                   GUID = issue.IdIssue,
                   Name = issue.NameIssue,
                   GUID_Parent = Config.LocalData.ClassRel_Issue__Jira__accessible_by_Url.ID_Class_Left,
                   Type = globals.Type_Object
               }, new clsOntologyItem
               {
                   GUID = issue.IdUrl,
                   Name = issue.Url,
                   GUID_Parent = Config.LocalData.ClassRel_Issue__Jira__accessible_by_Url.ID_Class_Right,
                   Type = globals.Type_Object
               },
               Config.LocalData.RelationType_accessible_by)));

               relationsToSave.AddRange(issuesToSave.Select(issue => relationConfig.Rel_ObjectRelation(syncSession,
                   issue, Config.LocalData.RelationType_contains)));

               relationsToSave.AddRange(projects.Select(proj => relationConfig.Rel_ObjectRelation(syncSession, proj, Config.LocalData.RelationType_belongs_to)));

               if (relationsToSave.Any())
               {
                   result.ResultState = dbConnector.SaveObjRel(relationsToSave);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while saving the relations of the issues!";
                       return result;
                   }
               }

               var attributesToSave = new List<clsObjectAtt>();

               attributesToSave.Add(relationConfig.Rel_ObjectAttribute(syncSession, Config.LocalData.AttributeType_Datetimestamp__Create_, syncStamp));

               result.ResultState = dbConnector.SaveObjAtt(attributesToSave);

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<IssueRelation>> GetIssueRelations(List<WorkItem> workItems, clsOntologyItem projectItem)
        {
            var taskResult = await Task.Run<ResultItem<IssueRelation>>(() =>
            {
                var result = new ResultItem<IssueRelation>
                {
                    ResultState = globals.LState_Error.Clone(),
                    Result = new IssueRelation()
                };

                var searchIssuesIds = workItems.Select(issue => new clsOntologyItem
                {
                    Name = issue.Id.ToString(),
                    GUID_Parent = Config.LocalData.Class_Task_Id.GUID
                }).ToList();

                if (!searchIssuesIds.Any()) return result;

                var dbReaderIssuesIds = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderIssuesIds.GetDataObjects(searchIssuesIds);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the issue-Ids!";
                    return result;
                }

                var searchIssues = (from id in dbReaderIssuesIds.Objects1
                                    join wi in workItems on id.Name equals wi.Id.ToString()
                                    select id).Select(id => new clsObjectRel
                                    {
                                        ID_Parent_Object = Config.LocalData.Class_Issue__Jira_ .GUID,
                                        ID_RelationType = Config.LocalData.RelationType_is_defined_by.GUID,
                                        ID_Other = id.GUID
                                    }).ToList();

                searchIssues.AddRange((from id in dbReaderIssuesIds.Objects1
                                      join wi in workItems on id.Name equals wi.Id.ToString()
                                      select id).Select(id => new clsObjectRel
                                      {
                                          ID_Parent_Object = Config.LocalData.Class_Issue__Archive_.GUID,
                                          ID_RelationType = Config.LocalData.RelationType_is_defined_by.GUID,
                                          ID_Other = id.GUID
                                      }));

                if (!searchIssues.Any()) return result;
                var dbReaderIssues = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderIssues.GetDataObjectRel(searchIssues);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting relations between issue-Ids and issues!";
                    return result;
                }

                var searchProjectToIssues = dbReaderIssues.ObjectRels.Select(rel => new clsObjectRel
                {
                    ID_Object = projectItem.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Project__TFS_Azure_DevOps__contains_Issue__Jira_.ID_RelationType,
                    ID_Other = rel.ID_Object
                }).ToList();

                var dbReaderProjectToIssues = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderProjectToIssues.GetDataObjectRel(searchProjectToIssues);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting relations between project and Issues!";
                    return result;
                }

                result.Result.IssuesToTaskIds = (from issueToTaskIdRel in dbReaderIssues.ObjectRels
                                                 join projectToIssue in dbReaderProjectToIssues.ObjectRels on issueToTaskIdRel.ID_Object equals projectToIssue.ID_Other
                                                 select issueToTaskIdRel).ToList();

                result.Result.ProjectsToIssues = dbReaderProjectToIssues.ObjectRels;

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveProjects(List<clsOntologyItem> projectList)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbWriter = new OntologyModDBConnector(globals);

                if (projectList.Any())
                {
                    result = dbWriter.SaveObjects(projectList);
                }


                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveReleases(List<ReleaseOItem> releaseList, clsOntologyItem projectItem)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbReader = new OntologyModDBConnector(globals);

                var searchReleases = releaseList.Select(rel => new clsOntologyItem
                {
                    GUID = rel.GUID
                }).ToList();

                if (searchReleases.Any())
                {
                    result = dbReader.GetDataObjects(searchReleases);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while searching releases";
                        return result;
                    }
                }

                var releaseListToSave = (from release in releaseList
                                         join exist in dbReader.Objects1 on release.GUID equals exist.GUID into exists
                                         from exist in exists.DefaultIfEmpty()
                                         where exist == null
                                         select release).GroupBy(rel => new { rel.GUID, rel.Name, rel.GUID_Parent, rel.Type }).Select(relGrp => new clsOntologyItem
                                         {
                                             GUID = relGrp.Key.GUID,
                                             Name = relGrp.Key.Name,
                                             GUID_Parent = relGrp.Key.GUID_Parent,
                                             Type = relGrp.Key.Type
                                         }).ToList();

                var dbWriter = new OntologyModDBConnector(globals);

                if (releaseListToSave.Any())
                {
                    result = dbWriter.SaveObjects(releaseListToSave);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving Releases";
                        return result;
                    }
                }

                var searchRels = releaseList.Select(rel => new clsObjectRel
                {
                    ID_Object = projectItem.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Project__TFS_Azure_DevOps__contains_Release.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Project__TFS_Azure_DevOps__contains_Release.ID_Class_Right
                }).ToList();

                if (searchRels.Any())
                {
                    result = dbReader.GetDataObjectRel(searchRels);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while searching projects-to-releases";
                        return result;
                    }
                }

                var relationConfig = new clsRelationConfig(globals);

                var releaseRelList = (from projToRel in releaseList.Select(rel => relationConfig.Rel_ObjectRelation(projectItem, rel, Config.LocalData.RelationType_contains))
                                      join projToRelExist in dbReader.ObjectRels on new { projToRel.ID_Object, projToRel.ID_RelationType, projToRel.ID_Other } equals new { projToRelExist.ID_Object, projToRelExist.ID_RelationType, projToRelExist.ID_Other } into projToRelExists
                                      from projToRelExist in projToRelExists.DefaultIfEmpty()
                                      where projToRelExist == null
                                      select projToRel).ToList();

                if (releaseRelList.Any())
                {
                    result = dbWriter.SaveObjRel(releaseRelList);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving relations between Projects and Releases";
                        return result;
                    }
                }

                var releaseDefinitions = releaseList.Select(rel => new ReleaseOItem { GUID = MD5Converters.CalculateMD5Hash($"releaseDef_{projectItem.GUID}_{rel.TFSRelease.ReleaseDefinitionReference.Id}"), Name = rel.TFSRelease.ReleaseDefinitionReference.Name, GUID_Parent = Config.LocalData.Class_Releasedefinition.GUID, Type = globals.Type_Object, TFSRelease = rel.TFSRelease, GUID_Related = rel.GUID }).ToList();

                var searchReleaseDefinitions = releaseDefinitions.Select(rel => new clsOntologyItem
                {
                    GUID = rel.GUID,
                    GUID_Parent = rel.GUID_Parent
                }).ToList();

                var dbReaderReleaseDefinitions = new OntologyModDBConnector(globals);
                if (searchReleaseDefinitions.Any())
                {
                    result = dbReaderReleaseDefinitions.GetDataObjects(searchReleaseDefinitions);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while searching ReleaseDefinitions";
                        return result;
                    }
                }

                var releaseDefinitionsToSave = (from releaseDef in releaseDefinitions
                                                join dbReleaseDef in dbReaderReleaseDefinitions.Objects1 on releaseDef.GUID equals dbReleaseDef.GUID into dbReleaseDefs
                                                from dbReleaseDef in dbReleaseDefs.DefaultIfEmpty()
                                                where dbReleaseDef == null
                                                select new
                                                {
                                                    GUID = releaseDef.GUID,
                                                    Name = releaseDef.Name,
                                                    GUID_Parent = releaseDef.GUID_Parent,
                                                    Type = releaseDef.Type
                                                }).GroupBy(rel => new { rel.GUID, rel.Name, rel.GUID_Parent, rel.Type }).Select(relGrp => new clsOntologyItem
                                                {
                                                    GUID = relGrp.Key.GUID,
                                                    Name = relGrp.Key.Name,
                                                    GUID_Parent = relGrp.Key.GUID_Parent,
                                                    Type = relGrp.Key.Type
                                                }).ToList();
                if (releaseDefinitionsToSave.Any())
                {
                    result = dbWriter.SaveObjects(releaseDefinitionsToSave);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving ReleaseDefinitions";
                        return result;
                    }
                }


                var releaseDefToReleases = releaseDefinitions.GroupBy(rel => new
                {
                    rel.GUID,
                    rel.Name,
                    rel.GUID_Parent,
                    rel.Type,
                    ReleaseName = rel.TFSRelease.Name,
                    ReleaseId = rel.GUID_Related
                }).Select(rel => relationConfig.Rel_ObjectRelation(new clsOntologyItem
                {
                    GUID = rel.Key.GUID,
                    Name = rel.Key.Name,
                    GUID_Parent = rel.Key.GUID_Parent,
                    Type = rel.Key.Type
                }, new clsOntologyItem
                {
                    GUID = rel.Key.ReleaseId,
                    Name = rel.Key.ReleaseName,
                    GUID_Parent = Config.LocalData.Class_Release.GUID,
                    Type = globals.Type_Object
                },
                    Config.LocalData.RelationType_contains)).ToList();

                var searchReleaseDefToReleases = releaseDefToReleases.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = rel.ID_RelationType,
                    ID_Other = rel.ID_Other
                }).ToList();

                var dbReaderReleaseDefToReleases = new OntologyModDBConnector(globals);

                if (searchReleaseDefToReleases.Any())
                {
                    result = dbReaderReleaseDefToReleases.GetDataObjectRel(searchReleaseDefToReleases);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while reading releasedefinitions to releases";
                        return result;
                    }
                }

                var relsToSave = (from releaseDefToRelease in releaseDefToReleases
                                  join releaseDefToReleaseDb in dbReaderReleaseDefToReleases.ObjectRels on new { releaseDefToRelease.ID_Object, releaseDefToRelease.ID_RelationType, releaseDefToRelease.ID_Other } equals
                                                                                                           new { releaseDefToReleaseDb.ID_Object, releaseDefToReleaseDb.ID_RelationType, releaseDefToReleaseDb.ID_Other } into releaseDefToReleaseDbs
                                  from releaseDefToReleaseDb in releaseDefToReleaseDbs.DefaultIfEmpty()
                                  where releaseDefToReleaseDb == null
                                  select releaseDefToRelease).ToList();

                if (relsToSave.Any())
                {
                    result = dbWriter.SaveObjRel(relsToSave);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving release-definitions to releases";
                        return result;
                    }
                }


                var projectsToReleaseDef = releaseDefinitions.GroupBy(rel => new
                {
                    rel.GUID,
                    rel.Name,
                    rel.GUID_Parent,
                    rel.Type,
                    ReleaseName = rel.TFSRelease.Name,
                    ReleaseId = rel.GUID_Related
                }).Select(rel => relationConfig.Rel_ObjectRelation(projectItem,
                    new clsOntologyItem
                    {
                        GUID = rel.Key.GUID,
                        Name = rel.Key.Name,
                        GUID_Parent = rel.Key.GUID_Parent,
                        Type = rel.Key.Type
                    },
                   Config.LocalData.RelationType_contains)).ToList();

                var searchProjectsToReleaseDef = projectsToReleaseDef.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_Other = rel.ID_Other,
                    ID_RelationType = rel.ID_RelationType
                }).ToList();

                var dbReaderProjectsToReleaseDefs = new OntologyModDBConnector(globals);

                if (searchProjectsToReleaseDef.Any())
                {
                    result = dbReaderProjectsToReleaseDefs.GetDataObjectRel(searchProjectsToReleaseDef);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while reading projects to release-definitions";
                        return result;
                    }
                }

                relsToSave = (from projectToReleaseDef in projectsToReleaseDef
                              join projectToReleaseDefDb in dbReaderProjectsToReleaseDefs.ObjectRels on new { projectToReleaseDef.ID_Object, projectToReleaseDef.ID_RelationType, projectToReleaseDef.ID_Other } equals
                                                                                                           new { projectToReleaseDefDb.ID_Object, projectToReleaseDefDb.ID_RelationType, projectToReleaseDefDb.ID_Other } into projectToReleaseDefDbs
                              from projectToReleaseDefDb in projectToReleaseDefDbs.DefaultIfEmpty()
                              where projectToReleaseDefDb == null
                              select projectToReleaseDef).ToList();
                if (relsToSave.Any())
                {
                    result = dbWriter.SaveObjRel(relsToSave);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving projects to release-definitions";
                        return result;
                    }
                }


                return result;
            });

            return taskResult;
        }

        public ElasticAgent(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
    }
}

