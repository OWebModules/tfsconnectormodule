﻿using Microsoft.VisualStudio.Services.ReleaseManagement.WebApi;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSConnectorModule.Models
{
    public class ReleaseOItem : clsOntologyItem
    {
        public Release TFSRelease { get; set; }
    }
}
