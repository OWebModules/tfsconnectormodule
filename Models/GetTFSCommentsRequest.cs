﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSConnectorModule.Models
{
    public class GetTFSCommentsRequest : IGetModelTFSConfig
    {
        public string IdConfig { get; set; }
        public string RegexComment { get; set; }

        public IMessageOutput MessageOutput { get; set; }

        public GetTFSCommentsRequest(string idConfig)
        {
            IdConfig = idConfig;
        }

    }
}
