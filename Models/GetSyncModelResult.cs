﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSConnectorModule.Models
{
    public class GetSyncModelResult
    {
        public SyncTFSRequest Request { get; set; }
        public List<clsOntologyItem> ConfigList { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> SubConfigList { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ConfigsToUrls { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ConfigsToPostFixUrls { get; set; } = new List<clsObjectRel>();
        public List<clsOntologyItem> Projects { get; set; } = new List<clsOntologyItem>();

        public List<clsObjectRel> ProjectsToReleases { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ChangeSetsToProjects { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConfigToProject { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConfigToAccessTokens { get; set; } = new List<clsObjectRel>();

        public List<clsObjectAtt> ConfigToGetWorkItems { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectAtt> ConfigToGetChangeSets { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectRel> ConfigsToAssignedTo { get; set; } = new List<clsObjectRel>();
    }
}
