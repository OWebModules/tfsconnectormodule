﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSConnectorModule.Models
{
    public class GetTFSCommentsResult
    {
        public List<ChangeSetDb> ChangeSets { get; set; } = new List<ChangeSetDb>();

    }
}
