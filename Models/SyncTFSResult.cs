﻿using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSConnectorModule.Models
{
    public class SyncTFSResult
    {
        public List<ResultItem<SyncReleaseResult>> ReleaseResults { get; set; } = new List<ResultItem<SyncReleaseResult>>();
        public List<ChangeSetDb> ChangeSetResults { get; set; } = new List<ChangeSetDb>();
    }

    public class SyncReleaseResult
    {
        public clsOntologyItem ConfigItem { get; set; }
        public clsOntologyItem UrlItem { get; set; }
    }
}
