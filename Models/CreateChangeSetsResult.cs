﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSConnectorModule.Models
{
    public class CreateChangeSetsResult
    {
        public List<ChangeSetDb> ChangeSets { get; set; } = new List<ChangeSetDb>();
    }

    public class ChangeSetDb
    {
        public clsOntologyItem ChangeSet { get; set; }
        public clsOntologyItem Url { get; set; }
        public clsObjectAtt Description { get; set; }

        public clsObjectAtt CreateDate { get; set; }
        public clsObjectRel RelToUrl { get; set; }
        public clsObjectRel RelToProject { get; set; }
    }
}
