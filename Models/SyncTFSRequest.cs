﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSConnectorModule.Models
{
    public class SyncTFSRequest : IGetModelTFSConfig
    {
        public string IdConfig { get; set; }
        public string Password { get; set; }

        public IMessageOutput MessageOutput { get; set; }

        public SyncTFSRequest(string idConfig)
        {
            IdConfig = idConfig;
        }

    }
}
