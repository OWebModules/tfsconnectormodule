﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSConnectorModule.Models
{
    public class IssueRelation
    {
        public List<clsObjectRel> IssuesToTaskIds { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ProjectsToIssues { get; set; } = new List<clsObjectRel>();
    }
}
