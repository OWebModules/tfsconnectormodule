﻿using Microsoft.TeamFoundation.Core.WebApi;
using Microsoft.TeamFoundation.SourceControl.WebApi;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSConnectorModule.Models
{
    public class CreateChangeSetsRequest
    {
        public clsOntologyItem Project { get; set; }
        public TeamProjectReference TFSProject { get; set; }
        public List<TfvcChangesetRef> ChangeSets { get; set; } = new List<TfvcChangesetRef>();
    }

    
}
