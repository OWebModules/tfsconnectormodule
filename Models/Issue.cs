﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSConnectorModule.Models
{
    public class Issue
    {
        public string IdProject { get; set; }
        public string NameProject { get; set; }
        public string IdIssue { get; set; }
        public string NameIssue { get; set; }
        public string IdTaskId { get; set; }
        public string NameTaskId { get; set; }

        public string IdUrl { get; set; }
        public string Url { get; set; }

    }
}
