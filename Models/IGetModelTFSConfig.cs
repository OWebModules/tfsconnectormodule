﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSConnectorModule.Models
{
    public interface IGetModelTFSConfig
    {
        string IdConfig { get; set; }
        IMessageOutput MessageOutput { get; set; }
    }
}
