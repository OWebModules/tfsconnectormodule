﻿using Microsoft.TeamFoundation.Build.WebApi;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.Core.WebApi;
using Microsoft.TeamFoundation.SourceControl.WebApi;
using Microsoft.TeamFoundation.WorkItemTracking.WebApi;
using Microsoft.TeamFoundation.WorkItemTracking.WebApi.Models;
using Microsoft.VisualStudio.Services.Client;
using Microsoft.VisualStudio.Services.Common;
using Microsoft.VisualStudio.Services.ReleaseManagement.WebApi.Clients;
using Microsoft.VisualStudio.Services.WebApi;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Converters;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TFSConnectorModule.Models;
using TFSConnectorModule.Services;

namespace TFSConnectorModule
{
    public class TFSConnector : AppController
    {
        public async Task<ResultItem<SyncTFSResult>> SyncTFS(SyncTFSRequest request)
        {
            

            var result = new ResultItem<SyncTFSResult>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new SyncTFSResult()
            };

            var elasticAgent = new ElasticAgent(Globals);
            request.MessageOutput?.OutputInfo("Get Model...");
            var serviceResult = await elasticAgent.GetSyncModel(request);
            
            if (serviceResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = serviceResult.ResultState;
                result.ResultState.Additional1 = $"Error Reading Config: {serviceResult.ResultState.Additional1}";
                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                return result;
            }
            request.MessageOutput?.OutputInfo("Have Model.");

            if (serviceResult.Result.ConfigToAccessTokens.Count > 1)
            {
                result.ResultState = Globals.LState_Error.Clone();
                result.ResultState.Additional1 = "You can only provide one or none access-token!";
                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                return result;
            }

            

            var taskResult = await Task.Run<ResultItem<SyncTFSResult>>(async() =>
            {
                result = new ResultItem<SyncTFSResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new SyncTFSResult()
                };

                var relationConfig = new clsRelationConfig(Globals);



                request.MessageOutput?.OutputInfo($"Found {serviceResult.Result.ConfigList.Count} Configs");
                foreach (var configItem in serviceResult.Result.ConfigList)
                {
                    request.MessageOutput?.OutputInfo($"Config: {configItem.Name}");
                    var getChangeSets = false;
                    var getChangeSetsRel = serviceResult.Result.ConfigToGetChangeSets.FirstOrDefault(att => att.ID_Object == configItem.GUID);
                    if (getChangeSetsRel != null)
                    {
                        getChangeSets = getChangeSetsRel.Val_Bit.Value;
                    }

                    var getWorkItems = false;
                    var getWorkItemsRel = serviceResult.Result.ConfigToGetWorkItems.FirstOrDefault(att => att.ID_Object == configItem.GUID);
                    if (getWorkItemsRel != null)
                    {
                        getWorkItems = getWorkItemsRel.Val_Bit.Value;
                    }

                    request.MessageOutput?.OutputInfo($"Get Changesets: {getChangeSets}; Get Workitems: {getWorkItems}");

                    var accessTokenRel = serviceResult.Result.ConfigToAccessTokens.Where(acc => acc.ID_Object == configItem.GUID).SingleOrDefault();

                    var accessToken = string.Empty;
                    if (accessTokenRel != null)
                    {
                        request.MessageOutput?.OutputInfo($"Work with Accesstoken");

                        if (string.IsNullOrEmpty(request.Password))
                        {
                            result.ResultState = Globals.LState_Error.Clone();
                            result.ResultState.Additional1 = "You have to provide a password for master-user to decode the access-token!";
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }

                        var secController = new SecurityModule.SecurityController(Globals);

                        request.MessageOutput?.OutputInfo($"Get Accesstoken...");
                        var resultGetPassword = await secController.GetPassword(configItem, request.Password, Config.LocalData.Class_Accesstoken.GUID);
                        result.ResultState = resultGetPassword.Result;

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while getting the password for decrypting the access-token!";
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }

                        accessToken = resultGetPassword.CredentialItems.First().Password.Name_Other;
                        request.MessageOutput?.OutputInfo($"Have Accesstoken.");
                    }

                    var releaseResult = new ResultItem<SyncReleaseResult>
                    {
                        ResultState = Globals.LState_Success.Clone(),
                        Result = new SyncReleaseResult()
                    };
                    result.Result.ReleaseResults.Add(releaseResult);
                    releaseResult.Result.ConfigItem = configItem;
                    releaseResult.Result.UrlItem = serviceResult.Result.ConfigsToUrls.Where(urlRel => urlRel.ID_Object == configItem.GUID).Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }).FirstOrDefault();

                    if (releaseResult.Result.UrlItem == null)
                    {
                        releaseResult.ResultState = Globals.LState_Error.Clone();
                        releaseResult.ResultState.Additional1 = "No Url found";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        continue;
                    }

                    var collectionUri = new Uri(releaseResult.Result.UrlItem.Name);

                    VssConnection connection = null;
                    request.MessageOutput?.OutputInfo($"Connect...");

                    if (string.IsNullOrEmpty(accessToken))
                    {
                        connection = new VssConnection(new Uri(releaseResult.Result.UrlItem.Name), new VssCredentials());
                    }
                    else
                    {
                        connection = new VssConnection(new Uri(releaseResult.Result.UrlItem.Name), new VssBasicCredential(string.Empty, accessToken));
                    }

                    request.MessageOutput?.OutputInfo($"Connected.");

                    request.MessageOutput?.OutputInfo($"Get Sourcecontrol, Releaseclient, Projectclient and Buildclient");
                    var sourceControlClient = connection.GetClient<TfvcHttpClient>();
                    var releaseClient = connection.GetClient<ReleaseHttpClient>();
                    var projectClient = connection.GetClient<ProjectHttpClient>();
                    var buildClient = connection.GetClient<BuildHttpClient>();


                    request.MessageOutput?.OutputInfo($"Get Projects...");
                    var projects = await projectClient.GetProjects();
                    var projects1 = (from project in projects
                                join configToProject in serviceResult.Result.ConfigToProject.Where(proj => proj.ID_Object == configItem.GUID) on project.Name equals configToProject.Name_Other into configToProjects
                                     from configToProject in configToProjects.DefaultIfEmpty()
                                select project).ToList();
                    var projectItems = projects1.Select(proj => 
                    {
                        var resultItem = new { Id = proj.Id.ToString().Replace("-", ""),
                            Project = proj };
                        return resultItem;
                    });

                    request.MessageOutput?.OutputInfo($"Have {projects1.Count}.");

                    var dbProjects = (from tfsProject in projectItems
                                        join dbProject in serviceResult.Result.Projects on tfsProject.Id equals dbProject.GUID into dbProjects1
                                        from dbProject in dbProjects1.DefaultIfEmpty()
                                        select new { tfsProject, dbProject }).Select(proj =>
                                        {
                                            var projectItem = proj.dbProject;
                                            if (proj.dbProject == null || proj.tfsProject.Project.Name != proj.dbProject.Name)
                                            {
                                                projectItem = new clsOntologyItem
                                                {
                                                    GUID = proj.tfsProject.Project.Id.ToString().Replace("-",""),
                                                    Name = proj.tfsProject.Project.Name,
                                                    GUID_Parent = Config.LocalData.Class_Project__TFS_Azure_DevOps_.GUID,
                                                    Type = Globals.Type_Object,
                                                    Mark = true
                                                };



                                            }
                                            var resultItem = new Tuple<TeamProjectReference, clsOntologyItem>(proj.tfsProject.Project,
                                                projectItem);
                                            
                                            

                                            return resultItem;
                                        });

                    var saveProjects = dbProjects.Where(dbProject => dbProject.Item2.Mark != null && dbProject.Item2.Mark.Value).Select(dbProject => dbProject.Item2).ToList();
                    if (saveProjects.Any())
                    {
                        request.MessageOutput?.OutputInfo($"Save projects: {saveProjects.Count}...");
                        result.ResultState = await elasticAgent.SaveProjects(saveProjects);
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving TFS-Projects";
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }
                        request.MessageOutput?.OutputInfo($"Saved projects.");
                    }

                    if (!serviceResult.Result.ConfigToProject.Any())
                    {
                        result.ResultState.Additional1 = $"Saved {saveProjects.Count} new projects and exit, because of missing assigned project to config!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    var projectsToSync = dbProjects;
                    if (serviceResult.Result.ConfigToProject.Any())
                    {
                        projectsToSync = (from dbProject in dbProjects
                                          join filterProj in serviceResult.Result.ConfigToProject on dbProject.Item2.GUID equals filterProj.ID_Other
                                          select dbProject);
                    }
                    var md5Hash = MD5.Create();

                    var postFixUrl = serviceResult.Result.ConfigsToPostFixUrls.FirstOrDefault(urlRel => urlRel.ID_Object == configItem.GUID);
                    foreach (var project in projectsToSync)
                    {
                        
                        request.MessageOutput?.OutputInfo($"Sync Project {project.Item1.Name}");
                        var releases = await releaseClient.GetReleasesAsync(project: project.Item1.Id);
                        var buildDefinitions = await buildClient.GetFullDefinitionsAsync(project: project.Item1.Id);
                        
                        request.MessageOutput?.OutputInfo($"{releases.Count} Releases; {buildDefinitions.Count} Builddefinitions");

                        var releaseItems = releases.Select(rel => new ReleaseOItem { GUID = MD5Converters.CalculateMD5Hash($"{project.Item1.Id}_{rel.ReleaseDefinitionReference.Id}_{rel.Id}"),
                            Name = rel.Name,
                            GUID_Parent = Config.LocalData.Class_Release.GUID,
                            Type = Globals.Type_Object,
                            TFSRelease = rel}).ToList();

                        var saveReleasOntoItems = releaseItems.Select(rel => rel).Where(rel => rel != null).ToList();

                        
                        result.ResultState = await elasticAgent.SaveReleases(saveReleasOntoItems, project.Item2);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }

                        if (getChangeSets)
                        {
                            var changeSets = await sourceControlClient.GetChangesetsAsync(project.Item1.Id);
                            request.MessageOutput?.OutputInfo($"{changeSets.Count} Changesets");

                            var changeSetsToCreate = (from changeSet in changeSets
                                                      join changeSetDb in serviceResult.Result.ChangeSetsToProjects on changeSet.ChangesetId.ToString() equals changeSetDb.Name_Object into changeSetsDb
                                                      from changeSetDb in changeSetsDb.DefaultIfEmpty()
                                                      where changeSetDb == null
                                                      select changeSet);

                            request.MessageOutput?.OutputInfo($"{changeSetsToCreate.Count()} Changesets to save");
                            if (changeSetsToCreate.Any())
                            {
                                var requestChangeSetCreate = new CreateChangeSetsRequest()
                                {
                                    Project = project.Item2,
                                    ChangeSets = changeSetsToCreate.ToList()
                                };

                                var saveResult = await elasticAgent.CreateChangeSets(requestChangeSetCreate);

                                result.ResultState = saveResult.ResultState;
                                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                {
                                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                    return result;
                                }

                                result.Result.ChangeSetResults.AddRange(saveResult.Result.ChangeSets);
                            }
                        }
                        


                        if (getWorkItems)
                        {
                            var assignedToName = serviceResult.Result.ConfigsToAssignedTo.First(assignedTo => assignedTo.ID_Object == configItem.GUID).Name_Other;
                            //create a wiql object and build our query
                            Wiql wiql = new Wiql()
                            {
                                Query = "Select * " +
                                        "From WorkItems " +
                                        //"Where [Work Item Type] = 'Bug' " +
                                        //"And [System.TeamProject] = '" + project.Item1.Name + "' " +
                                        "WHERE [System.TeamProject] = '" + project.Item1.Name + "' " +
                                        "And [System.State] <> 'Closed' " +
                                        "And [System.AssignedTo] = '" + assignedToName + "'" +
                                        "Order By [State] Asc, [Changed Date] Desc"
                            };

                            using (WorkItemTrackingHttpClient workItemTrackingHttpClient = new WorkItemTrackingHttpClient(new Uri(releaseResult.Result.UrlItem.Name), new VssBasicCredential(string.Empty, accessToken)))
                            {
                                //execute the query to get the list of work items in the results
                                WorkItemQueryResult workItemQueryResult = await workItemTrackingHttpClient.QueryByWiqlAsync(wiql);

                                var fieldNames = workItemQueryResult.Columns.Where(col => col.ReferenceName.StartsWith("System.")).Select(col => col.ReferenceName).ToArray();
                                var workItemIds = workItemQueryResult.WorkItems.Select(workItem => workItem.Id).ToList();
                                var total = workItemIds.Count();
                                var getCount = total > 200 ? 200 : total;
                                var workItems = new List<WorkItem>();
                                var pos = 0;
                                while (pos < total)
                                {
                                    var idsToGet = workItemIds.GetRange(pos, getCount);
                                    workItems.AddRange(await workItemTrackingHttpClient.GetWorkItemsAsync(idsToGet.ToArray(), fieldNames, workItemQueryResult.AsOf));
                                    pos += idsToGet.Count;
                                    getCount = total - pos;
                                    if (getCount > 200) getCount = 200;
                                }

                               
                                request.MessageOutput?.OutputInfo($"{workItems.Count} Workitems");

                                var issueResult = await elasticAgent.GetIssueRelations(workItems, project.Item2);

                                var url = releaseResult.Result.UrlItem.Name + postFixUrl.Name_Other;
                                var workItemsToSave = (from workItem in workItems
                                                       join issueRel in issueResult.Result.IssuesToTaskIds on workItem.Id.ToString() equals issueRel.Name_Other into issueRels
                                                       from issueRel in issueRels.DefaultIfEmpty()
                                                       where issueRel == null
                                                       select new Models.Issue
                                                       {
                                                           IdProject = project.Item2.GUID,
                                                           NameProject = project.Item2.Name,
                                                           IdIssue = Globals.NewGUID,
                                                           NameIssue = workItem.Fields["System.Title"].ToString(),
                                                           IdTaskId = Globals.NewGUID,
                                                           NameTaskId = workItem.Id.ToString(),
                                                           Url = url + workItem.Id
                                                       }).ToList();

                                var checkIssuesResult = await elasticAgent.CheckIssues(workItemsToSave, project.Item2);

                                result.ResultState = checkIssuesResult.ResultState;
                                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                {
                                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                    return result;
                                }


                                //some error handling                
                                //if (workItemQueryResult.WorkItems.Count() != 0)
                                //{
                                //    //need to get the list of our work item ids and put them into an array
                                //    List<int> list = new List<int>();
                                //    foreach (var item in workItemQueryResult.WorkItems)
                                //    {
                                //        list.Add(item.Id);
                                //    }
                                //    int[] arr = list.ToArray();

                                //    //build a list of the fields we want to see
                                //    string[] fields = new string[5];
                                //    fields[0] = "System.Id";
                                //    fields[1] = "System.Title";
                                //    fields[2] = "System.State";
                                //    fields[3] = "System.IterationPath";
                                //    fields[4] = "System.AssignedTo";

                                //    //get work items for the ids found in query
                                //    var workItems = await workItemTrackingHttpClient.GetWorkItemsAsync(arr, fields, workItemQueryResult.AsOf);

                                //    Console.WriteLine("Query Results: {0} items found", workItems.Count);

                                //    //loop though work items and write to console
                                //    foreach (var workItem in workItems)
                                //    {
                                //        Console.WriteLine("{0}          {1}                     {2}", workItem.Id, workItem.Fields["System.Title"], workItem.Fields["System.State"]);
                                //    }


                                //}

                            }
                        }

                    }
                    
                }

                return result;
            });
            

            return taskResult;
        }

        public async Task<ResultItem<GetTFSCommentsResult>> GetTFSComments(GetTFSCommentsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetTFSCommentsResult>>(async() =>
            {
                var result = new ResultItem<GetTFSCommentsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new GetTFSCommentsResult()
                };


                var elasticAgent = new ElasticAgent(Globals);
                var serviceResult = await elasticAgent.GetSyncModel(request);

                if (serviceResult.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState = serviceResult.ResultState;
                    result.ResultState.Additional1 = $"Error Reading Config: {serviceResult.ResultState.Additional1}";
                    return result;
                }

                if (!string.IsNullOrEmpty(request.RegexComment))
                {
                    try
                    {
                        var regex = new Regex(request.RegexComment);
                    }
                    catch (Exception ex)
                    {

                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Provided regex is not valid: {ex.Message}";
                        return result;
                    }
                }

                var serviceResultChangeSets = await elasticAgent.GetChangeSets(request);

                result.ResultState = serviceResultChangeSets.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.ChangeSets = serviceResultChangeSets.Result;

                return result;
            });

            return taskResult;
        }

        public TFSConnector(Globals globals) : base(globals)
        {
        }
    }
}
